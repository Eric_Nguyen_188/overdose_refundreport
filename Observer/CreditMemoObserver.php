<?php
namespace Overdose\RefundReport\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Creditmemo;
use Overdose\RefundReport\Model\RefundReportFactory;
use Overdose\RefundReport\Model\RefundReport;

/**
 * Class CreditMemoObserver
 * @package Overdose\RefundReport\Observer
 */
class CreditMemoObserver implements ObserverInterface
{
    /**
     * @var RefundReportFactory
     */
    protected $refundReportFactory;

    /**
     * CreditMemoObserver constructor.
     * @param RefundReportFactory $refundReportFactory
     */
    public function __construct(
        RefundReportFactory $refundReportFactory
    ) {
        $this->refundReportFactory = $refundReportFactory;
    }

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var Creditmemo $creditMemo */
        $creditMemo = $observer->getEvent()->getCreditmemo();
        /** @var RefundReport $odRefund */
        $odRefund   = $this->refundReportFactory->create();
        $order      = $creditMemo->getOrder();

        $data = [
            'order_id'      => $creditMemo->getOrderId(),
            'refund_id'     => $creditMemo->getId(),
            'refund_type'   => $this->checkRefundType($order) ?'partial' :'full',
            'refund_amount' => $creditMemo->getBaseGrandTotal()
        ];
        $odRefund->setData($data)->save();
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function checkRefundType($order)
    {
        return $order->canCreditmemo() || $order->getCreditmemosCollection()->getTotalCount() > 1;
    }
}
