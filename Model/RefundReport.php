<?php

namespace Overdose\RefundReport\Model;

use Magento\Framework\Model\AbstractModel;
use Overdose\RefundReport\Model\ResourceModel\RefundReport as ResourceModel;

/**
 * Class RefundReport
 * @package Overdose\RefundReport\Model
 */
class RefundReport extends AbstractModel
{
    const CACHE_TAG = 'od_refund_report';
    const KEY_ID    = 'entity_id';

    /**
     * Model cache tag for clear cache in after save and after delete
     *
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * @var string
     */
    protected $_eventPrefix = 'od_refund_report';

    /**
     * @var string
     */
    protected $_eventObject = 'od_refund_report';

    /**
     * @var string
     */
    protected $_idFieldName = self::KEY_ID;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    /**
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
