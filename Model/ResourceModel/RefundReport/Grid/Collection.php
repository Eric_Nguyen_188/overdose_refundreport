<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Mageplaza
 * @package   Mageplaza_BetterWishlist
 * @copyright Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license   https://www.mageplaza.com/LICENSE.txt
 */

namespace Overdose\RefundReport\Model\ResourceModel\RefundReport\Grid;

use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;
use Overdose\RefundReport\Model\ResourceModel\RefundReport;
use Psr\Log\LoggerInterface;
use Zend_Db_Expr;

/**
 * Class Collection
 * @package Overdose\RefundReport\Model\ResourceModel\RefundReport\Grid
 */
class Collection extends SearchResult
{
    /**
     * ID Field Name
     *
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'overdose_refund_grid_item_collection';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'grid_item_collection';

    protected $_selectedColumns;

    /**
     * @var ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * Collection constructor.
     *
     * @param EntityFactoryInterface $entityFactory
     * @param LoggerInterface $logger
     * @param FetchStrategyInterface $fetchStrategy
     * @param ManagerInterface $eventManager
     * @param ProductMetadataInterface $productMetadata
     * @param RequestInterface $request
     * @param string $mainTable
     * @param string $resourceModel
     *
     * @throws LocalizedException
     */
    public function __construct(
        EntityFactoryInterface $entityFactory,
        LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        ProductMetadataInterface $productMetadata,
        RequestInterface $request,
        $mainTable = 'od_refund_report',
        $resourceModel = RefundReport::class
    ) {
        $this->productMetadata = $productMetadata;
        $this->request         = $request;

        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
    }

    /**
     * @return $this|SearchResult
     */
    protected function _initSelect()
    {
        $tableName = $this->getMainTable();
        $this->getSelect()->from(['main_table' => $tableName], $this->_getSelectedColumns())
            ->joinLeft(
                ['so' => $this->getTable('sales_order')],
                'main_table.order_id = so.entity_id',
                ['customer_email', 'customer_firstname', 'base_grand_total', 'created_at', 'increment_id']
            )
            ->joinLeft(
                ['sp' => $this->getTable('sales_order_payment')],
                'main_table.order_id = sp.parent_id',
                ['method']
            );

        return $this;
    }

    /**
     * @param array|string $field
     * @param null $condition
     *
     * @return $this|Collection
     */
    public function addFieldToFilter($field, $condition = null)
    {
        switch ($field) {
            case 'entity_id':
                $field = 'main_table.entity_id';
                break;
        }

        return parent::addFieldToFilter($field, $condition);
    }

    /**
     * @return array
     */
    protected function _getSelectedColumns()
    {
        if (!$this->_selectedColumns) {
            $this->_selectedColumns = [
                'entity_id'          => new Zend_Db_Expr('main_table.entity_id'),
                'order_id'           => new Zend_Db_Expr('main_table.order_id'),
                'refund_id'          => new Zend_Db_Expr('main_table.refund_id'),
                'refund_type'        => new Zend_Db_Expr('main_table.refund_type'),
                'refund_amount'      => new Zend_Db_Expr('main_table.refund_amount'),
                'refund_create'      => new Zend_Db_Expr('main_table.created_at'),
                'order_create'       => new Zend_Db_Expr('so.created_at'),
                'customer_email'     => new Zend_Db_Expr('so.customer_email'),
                'customer_firstname' => new Zend_Db_Expr('so.customer_firstname'),
                'base_grand_total'   => new Zend_Db_Expr('so.base_grand_total'),
                'increment_id'       => new Zend_Db_Expr('so.increment_id'),
                'payment_type'       => new Zend_Db_Expr('sp.method')
            ];
        }

        return $this->_selectedColumns;
    }
}
