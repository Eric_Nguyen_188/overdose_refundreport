<?php
namespace Overdose\RefundReport\Model\ResourceModel\RefundReport;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Overdose\RefundReport\Model\RefundReport as Model;
use Overdose\RefundReport\Model\ResourceModel\RefundReport as ResourceModel;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = Model::KEY_ID;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}
