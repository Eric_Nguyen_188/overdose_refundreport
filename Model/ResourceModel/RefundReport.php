<?php
namespace Overdose\RefundReport\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Overdose\RefundReport\Model\RefundReport as Model;

/**
 * Class RefundReport
 * @package Overdose\RefundReport\Model\ResourceModel
 */
class RefundReport extends AbstractDb
{
    const MAIN_TABLE = 'od_refund_report';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::MAIN_TABLE, Model::KEY_ID);
    }
}
