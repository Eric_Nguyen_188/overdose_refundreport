<?php
namespace Overdose\RefundReport\Ui\Component\Listing\Column\Report;

use Magento\Framework\UrlInterface;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

/**
 * Class OrderUrl
 * @package Overdose\RefundReport\Ui\Component\Listing\Column\Report
 */
class OrderUrl extends Column
{
    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $url
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $url,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->url = $url;
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getName();
            foreach ($dataSource['data']['items'] as &$item) {
                $url = $this->url->getUrl('sales/order/view', ['order_id' => $item['order_id']]);
                $item[$fieldName] = '<a href="' . $url . '">' . $item[$fieldName] . '</a>';
            }
        }

        return $dataSource;
    }
}